package org.alcynse.service;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.junit.Assert;
import org.junit.Test;

public class ServiceRegistryTest {

  @Test
  public void testServiceTimeout() throws Exception {
    ServiceRegistry registry = new ServiceRegistry();
    Future<Service> serviceResponse = registry
        .getService("never.existed.Service");
    try {
      serviceResponse.get(1000, TimeUnit.MILLISECONDS);
    } catch (TimeoutException e) {
      System.out.println("Service not found. " + e.getMessage());
      return;
    }
    Assert.fail();
  }

  @Test
  public void testServicePreparing() throws Exception {
    long registerTime = 1000;
    long lastGetTime = 2000;
    ServiceRegistry registry = new ServiceRegistry();
    ServiceRegisterThread srt = new ServiceRegisterThread(new TestService(),
        registry, registerTime);
    Future<Service> serviceResponse = registry.getService(TestService.class
        .getCanonicalName());
    long starttime = System.currentTimeMillis();
    srt.start();
    try {
      serviceResponse.get(lastGetTime, TimeUnit.MILLISECONDS);
    } catch (TimeoutException e) {
      Assert.fail(e.getMessage());
    }
    long time = System.currentTimeMillis() - starttime;
    Assert.assertTrue(time < ((lastGetTime + registerTime) / 2));
    System.out.println(time + " ms");
  }

  @Test
  public void testServicePreparedAndRemoved() throws Exception {
    long waitForServiceTime = 1000;
    ServiceRegistry registry = new ServiceRegistry();
    registry.add(new TestService());
    Future<Service> serviceResponse = registry.getService(TestService.class
        .getCanonicalName());
    long starttime = System.currentTimeMillis();
    TestService srv = null;
    try {
      srv = (TestService) serviceResponse.get(waitForServiceTime,
          TimeUnit.MILLISECONDS);
    } catch (TimeoutException e) {
      Assert.fail(e.getMessage());
    }
    long time = System.currentTimeMillis() - starttime;
    System.out.println(time + " ms");
    registry.remove(srv);
    serviceResponse = registry.getService(TestService.class.getCanonicalName());
    try {
      serviceResponse.get(waitForServiceTime, TimeUnit.MILLISECONDS);
    } catch (TimeoutException e) {
      return;
    }
    Assert.fail("Service did not removed.");
  }

  private class ServiceRegisterThread extends Thread {
    private Service         service;
    private ServiceRegistry registry;
    private long            waitingTime;
    public ServiceRegisterThread(Service service, ServiceRegistry registry,
        long waitingTime) {
      this.service = service;
      this.registry = registry;
      this.waitingTime = waitingTime;
    }
    @Override
    public void run() {
      synchronized (this) {
        try {
          wait(waitingTime);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      registry.add(service);
    }
  }

}
