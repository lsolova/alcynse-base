package org.alcynse.service;

public class TestService implements Service {

  public TestService() {}

  @Override
  public String[] getPointNames() {
    return new String[] { getClass().getCanonicalName() };
  }

  @Override
  public String getName() {
    return getClass().getCanonicalName();
  }

}
