package org.alcynse.synapse;

import org.alcynse.exception.ItemAlreadyExistsException;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Laszlo Solova
 */
public class SynapseTest {

  @Test
  public void testDuplicatedRegistration() {
    TestExtensionPoint tep = new TestExtensionPoint();
    try {
      Synapse.add(tep);
    } catch (ItemAlreadyExistsException e) {
      // Do nothing
    }
    try {
      Synapse.add(new TestExtensionPoint());
    } catch (ItemAlreadyExistsException e) {
      System.out.println(e.getMessage());
      return;
    }
    Assert.fail();
  }

  @Test
  public void testInvalidExtension() {
    final TestExtensionPoint tep = new TestExtensionPoint();
    try {
      Synapse.add(tep);
    } catch (ItemAlreadyExistsException e) {
      // Do nothing
    }
    try {
      Synapse.add(new TestExtensionImplInvalidClass(tep));
    } catch (InvalidClassException e) {
      System.out.println(e.getMessage());
      return;
    }
    Assert.fail();
  }

  @Test
  public void testDelayedExtensionRegistration() {
    TestExtensionPoint tep = new TestExtensionPoint();
    Synapse.remove(tep);
    TestExtensionImplValidClass te1 = new TestExtensionImplValidClass(tep);
    SecondTestExtensionImplValidClass te2 = new SecondTestExtensionImplValidClass(
        tep);
    Synapse.add(te1);
    Synapse.add(tep);
    Assert.assertEquals(1, tep.getExtensionsCount());
    Synapse.add(te2);
    Assert.assertEquals(2, tep.getExtensionsCount());
    Synapse.remove(te1);
    Assert.assertEquals(1, tep.getExtensionsCount());
  }

}
