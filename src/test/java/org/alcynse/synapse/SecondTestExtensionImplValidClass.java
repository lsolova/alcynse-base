package org.alcynse.synapse;

/**
 * @author Laszlo Solova
 */
public class SecondTestExtensionImplValidClass implements SecondTestExtension {

  private ExtensionPoint ep;

  public SecondTestExtensionImplValidClass(ExtensionPoint ep) {
    this.ep = ep;
  }

  @Override
  public String[] getPointNames() {
    return new String[] { ep.getPointName() };
  }

  @Override
  public String toString() {
    return "SecondTestExtensionImplValidClass []";
  }

}
