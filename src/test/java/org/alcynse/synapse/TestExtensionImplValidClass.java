package org.alcynse.synapse;

/**
 * @author Laszlo Solova
 */
public class TestExtensionImplValidClass implements TestExtension {

  private ExtensionPoint ep;

  public TestExtensionImplValidClass(ExtensionPoint ep) {
    this.ep = ep;
  }

  @Override
  public String[] getPointNames() {
    return new String[] { ep.getPointName() };
  }

  @Override
  public String toString() {
    return "TestExtensionImplValidClass []";
  }

}
