package org.alcynse.synapse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Laszlo Solova
 */
public class TestExtensionPoint implements ExtensionPoint {

  private List<Extension> extensions = new ArrayList<>();

  @Override
  public void add(Extension extension) throws InvalidClassException {
    if (!(extension instanceof TestExtension)
        && !(extension instanceof SecondTestExtension))
      throw new InvalidClassException();
    extensions.add(extension);
    System.out.println("Extension registered: " + extension);
  }

  @Override
  public String getPointName() {
    return getClass().getCanonicalName();
  }

  @SuppressWarnings("unchecked")
  @Override
  public Class<? extends Extension>[] getRequiredClasses() {
    return (Class<? extends Extension>[]) new Class<?>[] { TestExtension.class,
        SecondTestExtension.class };
  }

  @Override
  public void remove(Extension extension) {
    extensions.remove(extension);
    System.out.println("Extension removed: " + extension);
  }

  public int getExtensionsCount() {
    return extensions.size();
  }

}
