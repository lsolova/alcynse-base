package org.alcynse.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;
import org.junit.Assert;
import org.junit.Test;

/**
 * <p>This is a simple test for the {@link ResourceResolver}.</p>
 * 
 * @author Laszlo Solova
 */
public class ResourceResolverTest {

  private static final String TEST_SEARCH_PATTERN = ".*resolver\\.test";

  @Test
  public void searchAndLoadTest() throws Exception {
    Pattern pattern = Pattern.compile(TEST_SEARCH_PATTERN);
    List<Resource> list = ResourceResolver.getResources(pattern, null);
    Assert.assertEquals(1, list.size());
    for (Resource resource : list) {
      File f = new File(resource.getFilename());
      Assert.assertTrue(f.exists());
      InputStream fis = ResourceResolver.getResourceStream(resource);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      int read = -1;
      while ((read = fis.read()) != -1) {
        baos.write(read);
      }
      fis.close();
      Assert.assertEquals("ResourceResolver found this file.", baos.toString());
    }
  }

}
