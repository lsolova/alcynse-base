package org.alcynse.service;

import org.alcynse.synapse.Extension;

/**
 * <p>The features in the Alcynse are implemented as one or more services.
 * Services are the central object of a feature, this works like a conductor and
 * access point.</p>
 * 
 * @author Laszlo Solova
 */
public interface Service extends Extension {
  /**
   * It gives back the system-wide unique name of the service. It is recommended
   * to use the canonical name of the service class.
   * 
   * @return The unique name of this service.
   */
  public String getName();
}
