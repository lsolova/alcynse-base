package org.alcynse.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import org.alcynse.synapse.Extension;
import org.alcynse.synapse.ExtensionPoint;
import org.alcynse.synapse.InvalidClassException;

/**
 * <p>The Service Registry is designed to handle all services used in the
 * Alcynse framework.</p>
 * 
 * @author Laszlo Solova
 */
public final class ServiceRegistry implements ExtensionPoint {

  private Map<String, Service> services        = new HashMap<String, Service>();
  private Object               servicesLock    = new Object();
  private Set<ServiceResponse> serviceRequests = new HashSet<ServiceResponse>();

  public ServiceRegistry() {}

  @Override
  public void add(Extension extension) {
    if (!(extension instanceof Service))
      throw new InvalidClassException("The extension is not a Service:"
          + extension.getClass().getSimpleName());

    Service srv = (Service) extension;
    synchronized (servicesLock) {
      services.put(srv.getName(), srv);
      Iterator<ServiceResponse> srvreqIterator = serviceRequests.iterator();
      while (srvreqIterator.hasNext()) {
        ServiceResponse resp = srvreqIterator.next();
        if (srv.getName().equals(resp.getServiceName())) {
          resp.setService(srv);
          srvreqIterator.remove();
        }
        else if (resp.isCancelled()) {
          srvreqIterator.remove();
        }
      }
    }
  }

  @Override
  public String getPointName() {
    return getClass().getCanonicalName();
  }

  @SuppressWarnings("unchecked")
  @Override
  public Class<? extends Extension>[] getRequiredClasses() {
    return (Class<? extends Extension>[]) new Class<?>[] { Service.class };
  }

  public Future<Service> getService(String serviceName) {
    ServiceResponse respose = new ServiceResponse(serviceName);
    synchronized (servicesLock) {
      if (services.containsKey(serviceName)) {
        respose.setService(services.get(serviceName));
      }
      else {
        serviceRequests.add(respose);
      }
    }
    return respose;
  }

  @Override
  public void remove(Extension extension) {
    if (!(extension instanceof Service))
      return;

    Service srv = (Service) extension;
    synchronized (servicesLock) {
      services.remove(srv.getName());
    }
  }

}
