package org.alcynse.exception;

/**
 * If an item are not available and this can cause side-effects.
 * 
 * @author Laszlo Solova
 */
public class ItemNotFoundException extends AlcynseException {

  private static final long serialVersionUID = 3712142348130185740L;

  public ItemNotFoundException() {}

  public ItemNotFoundException(String arg0, Throwable arg1, boolean arg2,
      boolean arg3) {
    super(arg0, arg1, arg2, arg3);
  }

  public ItemNotFoundException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  public ItemNotFoundException(String arg0) {
    super(arg0);
  }

  public ItemNotFoundException(Throwable arg0) {
    super(arg0);
  }

}
