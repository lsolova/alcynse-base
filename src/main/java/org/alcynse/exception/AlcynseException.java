package org.alcynse.exception;

/**
 * The parent exception of all exceptions thrown in Alcynse.
 * 
 * @author Laszlo Solova
 */
public class AlcynseException extends RuntimeException {
  private static final long serialVersionUID = 7041020091953641966L;

  public AlcynseException() {
    super();
  }

  public AlcynseException(String arg0, Throwable arg1, boolean arg2,
      boolean arg3) {
    super(arg0, arg1, arg2, arg3);
  }

  public AlcynseException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  public AlcynseException(String arg0) {
    super(arg0);
  }

  public AlcynseException(Throwable arg0) {
    super(arg0);
  }

}
