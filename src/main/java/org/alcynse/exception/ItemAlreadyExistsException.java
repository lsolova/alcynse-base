package org.alcynse.exception;

/**
 * If an item already exists and it is read only or overwriting can cause
 * side-effects.
 * 
 * @author Laszlo Solova
 */
public class ItemAlreadyExistsException extends AlcynseException {

  private static final long serialVersionUID = 2741982843892237697L;

  public ItemAlreadyExistsException() {}

  public ItemAlreadyExistsException(String arg0, Throwable arg1, boolean arg2,
      boolean arg3) {
    super(arg0, arg1, arg2, arg3);
  }

  public ItemAlreadyExistsException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  public ItemAlreadyExistsException(String arg0) {
    super(arg0);
  }

  public ItemAlreadyExistsException(Throwable arg0) {
    super(arg0);
  }

}
