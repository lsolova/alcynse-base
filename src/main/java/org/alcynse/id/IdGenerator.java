package org.alcynse.id;

import org.alcynse.exception.AlcynseException;

/**
 * <p>ID generator. It have to be thread safe.</p>
 * 
 * @author Laszlo Solova
 */
public interface IdGenerator {
  /**
   * It gives back the current ID. It can be different in different threads.
   * 
   * @return The current ID.
   */
  public String current();
  /**
   * It gives back the next ID. The method is thread safe, so it gives back no
   * same value twice.
   * 
   * @return The next ID.
   * @throws AlcynseException If something goes wrong.
   */
  public String next() throws AlcynseException;
}
