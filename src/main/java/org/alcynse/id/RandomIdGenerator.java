package org.alcynse.id;

import java.util.Random;
import org.alcynse.exception.AlcynseException;

/**
 * <p>A random ID generator implementation. It generate 32 character long IDs,
 * based on character array predefined.</p>
 * 
 * @author Laszlo Solova
 */
public class RandomIdGenerator implements IdGenerator {
  private Random              random    = new Random(System.currentTimeMillis());
  private ThreadLocal<String> lastId    = new ThreadLocal<String>();
  private char[]              usedchars = new String(
                                            "0123456789abcdefghijklmnopqrstuvwxyz")
                                            .toCharArray();
  @Override
  public String current() {
    return lastId.get();
  }

  @Override
  public String next() throws AlcynseException {
    char[] newid = new char[32];
    for (int i = 0; i < newid.length; i++)
      newid[i] = usedchars[random.nextInt(usedchars.length - 1)];
    String createdid = new String(newid);
    lastId.set(createdid);
    return createdid;
  }

}
