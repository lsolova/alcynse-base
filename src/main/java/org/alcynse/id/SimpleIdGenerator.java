package org.alcynse.id;

/**
 * <p>A simple ID generator implementation. It generate IDs from current
 * timestamp and a counter. The counter is stored in memory, so it will be
 * reseted every restart. The timestamp and the counter is converted to hexa,
 * so they are shorter.</p>
 * 
 * @author Laszlo Solova
 */
public class SimpleIdGenerator implements IdGenerator {

  private static Integer                 idCounter     = 0;
  private static Object                  idCounterLock = new Object();
  private InheritableThreadLocal<String> lastId        = new InheritableThreadLocal<String>();

  public String current() {
    return lastId.get();
  }

  public String next() {
    int counted = -1;
    synchronized (idCounterLock) {
      counted = idCounter++;
    }
    long nowTime = System.currentTimeMillis() / 1000;
    String newId = Long.toHexString(nowTime) + "."
        + Integer.toHexString(counted);
    lastId.set(newId);
    return newId;
  }

}
