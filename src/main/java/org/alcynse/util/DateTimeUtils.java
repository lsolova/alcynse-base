package org.alcynse.util;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * System wide DateTime manipulation utils.
 * 
 * @author Laszlo Solova
 */
public class DateTimeUtils {
  public static DateTimeFormatter getDayFormatter() {
    return DateTimeFormat.forPattern("yyyyMMdd");
  }
  public static DateTimeFormatter getSecondFormatter() {
    return DateTimeFormat.forPattern("yyyyMMddHHmmss");
  }
  public static DateTimeFormatter getMillisecondFormatter() {
    return DateTimeFormat.forPattern("yyyyMMddHHmmssSSS");
  }
  /**
   * It gives back the current timestamp formatted by the millisecond formatter
   * of this class.
   * 
   * @return Formatted current timestamp.
   */
  public static Long getNow() {
    return Long.valueOf(getMillisecondFormatter().print(
        System.currentTimeMillis()));
  }
}
