package org.alcynse.util;

public class Resource {
  private String filename;
  private String sourcePackName;

  public enum Source {
    FILE("file:"), JAR("jar:");

    private String prefix;
    private Source(String prefix) {
      this.prefix = prefix;
    }
    public String getPrefix() {
      return prefix;
    }
  }

  public Resource(String filename) {
    this.filename = filename;
  }

  public Resource(String sourcePackName, String filename) {
    this.sourcePackName = sourcePackName;
    this.filename = filename;
  }

  public String getFilename() {
    return filename;
  }
  public Source getSource() {
    return (sourcePackName == null) ? Source.FILE : Source.JAR;
  }
  public String getSourcePackName() {
    return sourcePackName;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(sourcePackName == null ? Source.FILE.getPrefix() : Source.JAR
        .getPrefix());
    if (sourcePackName != null)
      sb.append(sourcePackName).append(":");
    sb.append(filename);
    return sb.toString();
  }

}
