package org.alcynse.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import org.alcynse.util.Resource.Source;
import org.slf4j.LoggerFactory;

/**
 * <p>The ResourceResolver can find a resource defined by a pattern on the
 * classpath. This is the base of configuration and other file-based
 * services.</p>
 * 
 * @author Laszlo Solova
 */
public class ResourceResolver {

  /**
   * It gives back a collection of resources defined by a regex pattern from all
   * classpath (java.class.path) elements. The pattern = Pattern.compile(".*");
   * gets all resources.
   * 
   * @param pattern The matching pattern.
   * @param directory An additional directory, where the resources are searched
   *          in.
   * @return The List of resources or an empty List.
   */
  public static List<Resource> getResources(Pattern pattern, String directory) {
    List<Resource> retval = new ArrayList<Resource>();

    String classPath = System.getProperty("java.class.path", ".")
        + ((directory != null) ? File.pathSeparator + directory : "")
        + File.pathSeparator;
    LoggerFactory.getLogger(ResourceResolver.class).trace(
        "Classpath:" + classPath);
    String[] classPathElements = classPath.split(File.pathSeparator);
    for (String element : classPathElements) {
      File file = new File(element);
      if (file.exists()) {
        if (file.isDirectory())
          retval.addAll(getResourcesFromDirectory(file, pattern));
        else
          retval.addAll(getResourcesFromJarFile(file, pattern));
      }
    }
    return retval;
  }

  /**
   * It comes back with the InputStream of the resource given.
   * 
   * @param resourcePath The path of the resource - it have to contain a Source
   *          prefix. This resource path can be one of the result by
   *          getResources(Pattern, String);
   * @return The InputStream associated to the resource.
   * @throws Exception
   */
  public static InputStream getResourceStream(Resource resourcePath)
      throws Exception {
    if (resourcePath.getSource() == Source.JAR)
      return ResourceResolver.class.getResourceAsStream((resourcePath
          .getFilename().startsWith("/") ? "" : "/")
          + resourcePath.getFilename());
    else if (resourcePath.getSource() == Source.FILE)
      return new FileInputStream(new File(resourcePath.getFilename()));
    throw new IllegalArgumentException(
        "Resource patch could not be resolved - not supported protocol:"
            + resourcePath);
  }

  private static List<Resource> getResourcesFromJarFile(File file,
      Pattern pattern) {
    List<Resource> retval = new ArrayList<Resource>();
    ZipFile zf;
    try {
      zf = new ZipFile(file);
    } catch (ZipException e) {
      throw new Error("Error zip reading: " + file.getAbsolutePath(), e);
    } catch (IOException e) {
      throw new Error("Error zip reading: " + file.getAbsolutePath(), e);
    }
    Enumeration<? extends ZipEntry> e = zf.entries();
    while (e.hasMoreElements()) {
      ZipEntry ze = e.nextElement();
      String fileName = ze.getName();
      boolean accept = pattern.matcher(fileName).matches();
      if (accept) {
        retval.add(new Resource(zf.getName(), fileName));
      }
    }
    try {
      zf.close();
    } catch (final IOException e1) {
      throw new Error(e1);
    }
    return retval;
  }

  private static List<Resource> getResourcesFromDirectory(File directory,
      Pattern pattern) {
    List<Resource> retval = new ArrayList<Resource>();
    File[] fileList = directory.listFiles();
    for (File file : fileList) {
      if (file.getName().endsWith(".jar")) {
        retval.addAll(getResourcesFromJarFile(file, pattern));
      }
      else if (file.isDirectory()) {
        retval.addAll(getResourcesFromDirectory(file, pattern));
      }
      else {
        try {
          String fileName = file.getName();
          boolean accept = pattern.matcher(fileName).matches();
          if (accept)
            retval.add(new Resource(file.getCanonicalPath()));
        } catch (IOException e) {
          throw new Error("Error directory reading: " + file.getAbsolutePath(),
              e);
        }
      }
    }
    return retval;
  }

}
