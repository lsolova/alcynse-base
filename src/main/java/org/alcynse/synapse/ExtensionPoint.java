package org.alcynse.synapse;

/**
 * <p>This is a provided service, interface or observation point, where other
 * Alcynse system elements can connect to. It can define the class of the
 * implementation, which can be handled.</p>
 * 
 * @author Laszlo Solova
 */
public interface ExtensionPoint extends SynapseItem {
  /**
   * Add a new extension.
   * 
   * @param extension The {@link Extension}, which should to be added.
   * @throws InvalidClassException If the Extension does not implement the
   *           required class.
   */
  public void add(Extension extension) throws InvalidClassException;
  /**
   * The name of this point. It have to be unique in the whole system. To use
   * the canonical class name recommended.
   * 
   * @return The point name.
   */
  public String getPointName();
  /**
   * The classes from which at least one should be implemented by an
   * {@link Extension}. If there is no such requirement, then it can be null.
   * 
   * @return The required classes or null if there are no such items.
   */
  public Class<? extends Extension>[] getRequiredClasses();
  /**
   * Remove an extension. If an extension is unregistered, then it can be
   * removed. The remove have to be done as soon as possible.
   * 
   * @param extension The {@link Extension} to remove.
   */
  public void remove(Extension extension);
}
