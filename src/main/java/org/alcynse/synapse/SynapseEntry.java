package org.alcynse.synapse;

class SynapseEntry<T extends SynapseItem> {

  private boolean associated;
  private T       entryObject;
  private String  pointName;

  public SynapseEntry(String pointName, T entryObject) {
    this.entryObject = entryObject;
    this.pointName = pointName;
  }

  public T getEntryObject() {
    return entryObject;
  }

  public String getPointName() {
    return pointName;
  }

  public boolean isAssociated() {
    return associated;
  }

  public void setAssociated(boolean associated) {
    this.associated = associated;
  }

  @Override
  public boolean equals(Object obj) {
    return (obj instanceof SynapseEntry<?> && entryObject
        .equals(((SynapseEntry<?>) obj).getEntryObject()));
  }

  @Override
  public int hashCode() {
    return entryObject.hashCode();
  }

}
