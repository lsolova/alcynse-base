package org.alcynse.synapse;

/**
 * <p>This can be a service add-on, a specialized implementation or a listener,
 * which can extend functionality of the pointed item.</p>
 * 
 * @author Laszlo Solova
 */
public interface Extension extends SynapseItem {
  /**
   * It gives back the name of all {@link ExtensionPoint}s, which this extension
   * can connected to.
   */
  public String[] getPointNames();
}
