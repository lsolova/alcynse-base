package org.alcynse.synapse;

import org.alcynse.exception.AlcynseException;

/**
 * <p>This is thrown when the {@link Extension} does not implements the class
 * required by the {@link ExtensionPoint}.</p>
 * 
 * @author Laszlo Solova
 */
public class InvalidClassException extends AlcynseException {

  private static final long serialVersionUID = -9065205305894232061L;

  public InvalidClassException() {}

  public InvalidClassException(String arg0, Throwable arg1, boolean arg2,
      boolean arg3) {
    super(arg0, arg1, arg2, arg3);
  }

  public InvalidClassException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  public InvalidClassException(String arg0) {
    super(arg0);
  }

  public InvalidClassException(Throwable arg0) {
    super(arg0);
  }

}
