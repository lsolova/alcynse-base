package org.alcynse.synapse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.alcynse.exception.ItemAlreadyExistsException;
import org.slf4j.LoggerFactory;

/**
 * <p>The Synapse is base function of Alcynse. This provides a dynamic binding
 * between system parts. It contains all {@link SynapseItem}s, which can be on
 * both end of a connection.</p>
 * <p>The registration order is irrelevant. If an extension point is
 * registered, then all previously associated extension will be registered into
 * this extension point.</p>
 * 
 * @author Laszlo Solova
 */
public final class Synapse {

  private static Map<String, SynapseEntry<ExtensionPoint>>  extensionPoints         = new HashMap<String, SynapseEntry<ExtensionPoint>>();
  private static Map<String, List<SynapseEntry<Extension>>> extensions              = new HashMap<String, List<SynapseEntry<Extension>>>();
  private static Object                                     synapseModificationLock = new Object();

  private Synapse() {}

  /**
   * Add an item to the Synapse registry.
   * 
   * @param item The {@link SynapseItem} object.
   * @throws InvalidClassException If there is known the required class of
   *           the {@link ExtensionPoint} associated to, then this exception can
   *           be thrown. This is for information purposes only.
   * @throws ItemAlreadyExistsException All extension points have to be
   *           registered with a unique name. If the name is in use, then this
   *           exception will be thrown.
   */
  public static void add(SynapseItem item) throws InvalidClassException,
      ItemAlreadyExistsException {
    addExtensionPoint(item);
    addExtension(item);
  }
  /**
   * Remove an item from the Synapse registry.
   * 
   * @param item The {@link SynapseItem} object to remove.
   */
  public static void remove(SynapseItem item) {
    removeExtensionPoint(item);
    removeExtension(item);
  }

  // /**
  // * Remove an item from the Synapse registry.
  // *
  // * @param name The name of {@link SynapseItem} object to remove.
  // */
  // public static void remove(String name) {
  //
  // }

  /**
   * Add an extension to the Synapse registry.
   * 
   * @param extension The {@link Extension} object.
   * @throws InvalidClassException If there is known the required class of the
   *           {@link ExtensionPoint} associated to, then this exception can be
   *           thrown. This is for information purposes only.
   */
  private static void addExtension(SynapseItem item)
      throws InvalidClassException {
    if (!(item instanceof Extension))
      return;
    Extension extension = (Extension) item;
    String[] pointNameA = extension.getPointNames();
    for (String pointName : pointNameA) {
      // Check point validity
      if (extensionPoints.containsKey(pointName)) {
        isExtensionClassValid(extension, pointName);
      }
      // Create extension entry
      SynapseEntry<Extension> extEntry = new SynapseEntry<Extension>(pointName,
          extension);
      synchronized (synapseModificationLock) {
        // Add to extensions list
        List<SynapseEntry<Extension>> extList = extensions.get(pointName);
        if (extList == null) {
          extList = new ArrayList<>();
          extensions.put(pointName, extList);
        }
        extList.add(extEntry);
        // Add to extension point
        if (extensionPoints.get(pointName) != null) {
          extensionPoints.get(pointName).getEntryObject().add(extension);
          extEntry.setAssociated(true);
        }
      }
    }
  }

  /**
   * Add an extension point to the Synapse registry.
   * 
   * @param extpoint The {@link ExtensionPoint} object.
   * @throws ItemAlreadyExistsException All extension points have to be
   *           registered with a unique name. If the name is in use, then this
   *           exception will be thrown.
   */
  private static void addExtensionPoint(SynapseItem item)
      throws ItemAlreadyExistsException {
    if (!(item instanceof ExtensionPoint))
      return;
    ExtensionPoint extpoint = (ExtensionPoint) item;
    String pointName = extpoint.getPointName();
    if (extensionPoints.containsKey(pointName)) {
      throw new ItemAlreadyExistsException("The ExtensionPoint with name '"
          + pointName + "' already exists.");
    }
    synchronized (synapseModificationLock) {
      extensionPoints.put(pointName, new SynapseEntry<ExtensionPoint>(
          pointName, extpoint));
      if (extensions.containsKey(pointName)) {
        for (SynapseEntry<Extension> extSE : extensions.get(pointName)) {
          if (!extSE.isAssociated()) {
            try {
              extpoint.add(extSE.getEntryObject());
              extSE.setAssociated(true);
            } catch (InvalidClassException ice) {
              LoggerFactory.getLogger(Synapse.class).warn(
                  "Invalid extension found on extension point registration.",
                  ice);
            }
          }
        }
      }
    }
  }

  /**
   * Removes an existing extension from the Synapse registry and from the
   * extension point.
   * 
   * @param extension The {@link Extension} to remove.
   */
  private static void removeExtension(SynapseItem item) {
    if (!(item instanceof Extension))
      return;
    Extension extension = (Extension) item;
    String[] pointNamesA = extension.getPointNames();
    synchronized (synapseModificationLock) {
      for (String pointName : pointNamesA) {
        if (extensions.containsKey(pointName))
          extensions.get(pointName).remove(
              new SynapseEntry<Extension>(pointName, extension));
        if (extensionPoints.containsKey(pointName))
          extensionPoints.get(pointName).getEntryObject().remove(extension);
      }
    }
  }

  /**
   * Removes an existing extension point.
   * 
   * @param extpoint The {@link ExtensionPoint} to remove.
   */
  private static void removeExtensionPoint(SynapseItem item) {
    if (!(item instanceof ExtensionPoint))
      return;
    ExtensionPoint extpoint = (ExtensionPoint) item;
    String pointName = extpoint.getPointName();
    synchronized (synapseModificationLock) {
      extensionPoints.remove(pointName);
      if (extensions.containsKey(pointName)) {
        for (SynapseEntry<Extension> extSE : extensions.get(pointName)) {
          /*
           * This is required to avoid calls from old extension point after
           * removed.
           */
          extpoint.remove(extSE.getEntryObject());
          extSE.setAssociated(false);
        }
      }
    }
  }

  /**
   * Check if an extensionpoint can handle the extension class or not.
   * 
   * @param extension The extension to check.
   * @param pointName The extension point name.
   */
  private static void isExtensionClassValid(Extension extension,
      String pointName) {
    Class<? extends Extension>[] extensionClasses = extensionPoints
        .get(pointName).getEntryObject().getRequiredClasses();
    if (extensionClasses != null) {
      boolean isValidClass = false;
      for (Class<? extends Extension> extensionClass : extensionClasses) {
        if (extensionClass.isInstance(extension)) {
          isValidClass = true;
          break;
        }
      }
      if (!isValidClass) {
        StringBuilder sb = new StringBuilder();
        for (Class<? extends Extension> extensionClass : extensionClasses) {
          sb.append(extensionClass.getCanonicalName()).append(",");
        }
        throw new InvalidClassException("The extension "
            + extension.getClass().getCanonicalName() + " does not implements "
            + sb.substring(0, sb.length() - 1) + ".");
      }
    }
  }

}
