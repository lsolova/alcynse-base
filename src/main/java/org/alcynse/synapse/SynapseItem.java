package org.alcynse.synapse;

/**
 * <p>Such an item can registered into the Synapse.</p>
 * 
 * @author Laszlo Solova
 */
public interface SynapseItem {}
